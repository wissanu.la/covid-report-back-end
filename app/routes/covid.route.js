module.exports = (app,route_version) => {
    const covids = require("../controllers/covid.controller");
    var router = require("express").Router();

    router.get("/new_case_per_date",covids.getReportCovidNewCasePerDateByDate);
    router.get("/new_case_per_date_in_month",covids.getReportCovidNewCasePerDateInMonthByMonthAndYear);
    router.get("/new_case_top_5_province",covids.getReportCovidNewCaseTop5ProvinceByDate);

    app.use(`${route_version}/covid/`,router);
}